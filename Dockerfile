FROM ruby:2.3.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /myapp
WORKDIR /myapp
COPY Gemfile /myapp/Gemfile
COPY Gemfile.lock /myapp/Gemfile.lock
RUN bundle check || bundle install
COPY . /myapp
COPY config/puma.rb config/puma.rb
CMD bundle exec puma -C config/puma.rb

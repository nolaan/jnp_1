# README

Aby uruchomić aplikację:

1. Wypakuj projekt
2. użyj komendy 'docker-compose build web'
3. następnie 'docker-compose up web'

lub

oddzielnie 'docker-compose up rabbitmq/mongo/redis/elasticsearch', zdarzały mi się wyścigi pomiędzy rabbitmq a aplikacją, a nie zrobiłem healthchecka sprawdzającego czy rabbitmq już stoi,
chociaż teraz jak testuję to tego nie ma, na wszelki wypadek napiszę

4. 'docker-compose run web rails db:seed' - zapełnienie bazy danymi graczy, 10k graczy

5. w oddzielnym oknie użyj komendy 'docker-compose run web rake sneakers:run' - worker konsumujący wiadomości z rabbitmq

6. w przeglądarce wpisz 'localhost:3000'


Technologie użyte:

- główna aplikacja: Ruby on Rails
- redis: użyty na /players, odświeżany przy rejestracji nowego użytkownika lub zatwierdzeniu nowej gry, gem 'redis'
- mongoDB: NoSQL, gem 'mongoid' w celu integracji z mongoDB
- elasticsearch: na głownej stronie można wyszukać graczy po mailu, działa 'fuzzy' search, dobry przykład to 'asd', wyszukuje też graczy z mailem 'asa', gem 'chewy'
- rabbitmq: używany po zatwierdzeniu gry lub po rejestracji nowego gracza, uaktualnia dane trzymane w Redisie, użyłem gemów 'bunny' i 'sneakers'


Wymagana jest posiadanie konta do grania i oglądania czegokolwiek
Graczy wyzywa innego gracza, jeden z nich wprowadza potem wynik, a drugi zatwierdza wynik

Koszulka z Eredinem!

Kamil Cywiński

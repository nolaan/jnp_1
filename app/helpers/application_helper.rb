module ApplicationHelper
  def askredis(msg)
    conn = Bunny.new(host: 'rabbitmq', automatically_recover: false)
    conn.start
    ch   = conn.create_channel
    q    = ch.queue("redis_queue", :durable => true)
    q.publish(msg, :persistent => true)
    puts " [x] Sent #{msg}"
    conn.close
  end
end

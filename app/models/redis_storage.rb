class RedisStorage
  include Singleton

  def get_players
    users = redis.get('users') || update_players
  end

  def update_players
    p 'redis_update'
    users = User.all.to_json
    redis.set('users', users)
  end

  private

  def redis
    @redis ||= Redis.new(host: 'redis', port: '6379')
  end
end

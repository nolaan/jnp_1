class Game
  include Mongoid::Document
  field :result, type: String
  field :challenger_accepted, type: Boolean, default: false
  field :challenged_accepted, type: Boolean, default: false
  belongs_to :winner, class_name: 'User', optional: true
  belongs_to :challenger, class_name: 'User'
  belongs_to :challenged, class_name: 'User'
  has_and_belongs_to_many :users

  def end_game!
    game = EloRating::Match.new
    users.each do |user|
      game.add_player(rating: user.elo_rating, winner: winner == user)
    end
    new_ratings = game.updated_ratings
    users.each_with_index do |user, i|
      user.update(elo_rating: new_ratings[i])
    end
  end
end

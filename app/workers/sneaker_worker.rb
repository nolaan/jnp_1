class SneakerWorker < ActiveJob::Base
  include Sneakers::Worker
  from_queue 'redis_queue'

  def work(msg)
    puts msg
    if msg == 'update_players'
      RedisStorage.instance.update_players
    end
    ack!
  end
end

class UsersIndex < Chewy::Index
  define_type User do
    field :email, type: 'text'
    field :elo_rating, type: 'integer'
    field :games_won, value: ->(user) { user.games.where(winner: user).count }, type: 'integer'
  end
end

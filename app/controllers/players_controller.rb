class PlayersController < ApplicationController
  before_action :authenticate_user!
  def index
    @users = users_from_redis
  end

  private

  def users_from_redis
    users = RedisStorage.instance.get_players
    JSON.load(users)
  end
end

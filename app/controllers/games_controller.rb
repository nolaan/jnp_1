class GamesController < ApplicationController
  include ApplicationHelper
  before_action :authenticate_user!
  def index
    @games = Game.all
  end

  def new
    challenged = User.find_by(email: params[:email])
    if challenged == current_user
      flash[:error] = "Can't challenge yourself"
      redirect_to players_path and return
    end
    game = Game.new
    game.users << current_user
    game.users << challenged
    game.challenger = current_user
    game.challenged = challenged
    game.save
    redirect_to pending_games_path
  end

  def pending
    @pending_result = current_user.games.where(:result => nil)
    @pending_accept = current_user
                        .games
                        .where(:result.ne => nil)
                        .or(
                          {:challenger => current_user, :challenger_accepted => false},
                          {:challenged => current_user, :challenged_accepted => false})
  end

  def send_result
    game = Game.find_by(id: params[:id])
    if game.result
      flash[:error] = 'already has a result'
      redirect_to pending_games_path and return
    end
    game.result = "#{params[:challenger_result]}:#{params[:challenged_result]}"
    game.challenger == current_user ? game.challenger_accepted = true : game.challenged_accepted = true
    game.save
    redirect_to pending_games_path
  end

  def accept_result
    game = Game.find_by(id: params[:id])
    game.challenger == current_user ? game.challenger_accepted = true : game.challenged_accepted = true
    challenger_score, challenged_score = game.result.split(':').map(&:to_i)
    game.winner = challenger_score > challenged_score ? game.challenger : game.challenged
    game.save
    game.end_game!
    askredis('update_players')
    redirect_to games_path
  end
end

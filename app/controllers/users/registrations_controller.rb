# frozen_string_literal: true
module Users
  class RegistrationsController < Devise::RegistrationsController
    include ApplicationHelper
    def create
      super do |resource|
        askredis('update_players')
      end
    end
  end
end

# frozen_string_literal: true
class UsersIndicesController < ApplicationController
  def index
    email = params[:email]
    @search = email
    @users = UsersIndex.query(match: {email: {query: email, fuzziness: 'AUTO'}})
  end
end

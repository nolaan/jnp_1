# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Chewy.logger = nil
Rails.application.initialize!

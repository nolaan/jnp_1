Rails.application.routes.draw do
  devise_for :users, controllers: {
    registrations: 'users/registrations'
  }

  root 'home#index'

  get 'players', to: 'players#index'
  post 'challenge', to: 'games#new'

  get 'users_search' => 'users_indices#index'

  resources :games, only: [:index] do
    get 'pending', on: :collection
    post 'send_result', on: :member
    post 'accept_result', on: :member
  end

end
